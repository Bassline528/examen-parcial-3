<?php 

class Book {

    private string $title;
    private $type;
    private string $editorial;
    private int $year;
    private $isbn;
    private $authors = array();

    public function __construct(string $title, $type, string $editorial, int $year, $isbn, $authors ) {
        $this->title = $title;
        $this->type = $type;
        $this->editorial = $editorial;
        $this->year = $year;
        $this->isbn = $isbn;
        $this->authors = $authors;
    }

    public function getTitle() {
        return $this->title;
    }

    public function getType() {
        return $this->type;
    }

    public function getEditorial() {
        return $this->editorial;
    }

    public function getYear() {
        return $this->year;
    }

    public function getISBN() {
        return $this->isbn;
    }

    public function getAuthors() {
        return $this->authors;
    }


    
}
