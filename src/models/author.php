<?php

class Author {

    private string $name;
    private string $nationality;
    private $birthDate;

    
    public function __construct(string $name, string $nationality, $birthDate) {
        $this->name = $name;
        $this->nationality = $nationality;
        $this->birthDate = $birthDate;
    }

    public function getName() {
        return $this->name;
    }
    public function getNationality() {
        return $this->nationality;
    }
    public function getBirthDate() {
        return $this->birthDate;
    }

}