<?php

include_once 'models/book.php';
include_once 'models/author.php';

$author1 = new Author("Nelson Aranda", "Paraguayo", date('d-m-Y', strtotime('25-08-1998')));
$author2 = new Author("Renato Ferrer", "Paraguayo", date('d-m-Y', strtotime('13-02-1999')));
$author3 = new Author("Mathias Lugo", "Paraguayo", date('d-m-Y', strtotime('01-09-1997')));

$book1 = new Book("PHP FOR DOOMIES 2025 EDITION", "PROGRAMACION", "TRIO UK EDITORIAL", 2022, "0-940016-73-7", array($author1, $author2, $author3));

$authorsList = $book1->getAuthors();
$bookName = $book1->getTitle();

echo("Los autores del libro $bookName son: \n");
foreach ($authorsList as $key => $value) {
    $authorName = $value->getName();
    echo(" - $authorName \n");
}


