
#   Examen parcial 3 

Este es el repositorio del examen parcial 3 de Ingenieria del Software 4, en el se encuentran los temas 3 y 4.

## Tema 3

Los archivos del tema 3 son los siguientes:
```
index.php
src/models/author.php
src/models/book.php
```
## Tema 4
Los archivos del tema 4 se encuentran en la siguiente ruta:
```
src/tema4
```
## Como levantar el proyecto

Para correr el proyecto utilice la extension **PHP DEBUGGER** de vscode "Launch Built-in web server" o simplemente si tiene instalado APACHE o NGINX o XAMPP clonar el repositorio en la carpeta correspondiente para que pueda ser accedido desde el navegador.

```
$ mkdir examen_nelson
$ cd examen_nelson
$ git clone https://gitlab.com/Bassline528/examen-parcial-3.git
$ composer install
```
### Para ver el tema 3

Luego de clonar el repositorio y levantar el proyecto acceder a la siguiente URL

Nota: en mi caso mi web server corre en el puerto 42847 por eso en los ejemplos muestra ese puerto.
```
http://localhost:42847/src/index.php
```

### Para el tema 4
```
http://localhost:42847/src/tema4/page1.php
```

## Authors

- [@Bassline528](https://gitlab.com/Bassline528)

